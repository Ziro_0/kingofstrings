import { Component } from '@angular/core';
import { Platform, MenuController, NavController, NavParams } from 'ionic-angular';
import { kosCreateConfigData } from '../../kingOfStrings/classes/data/kosConfigValidate';
import { Game } from '../../kingOfStrings/game';
import Listeners from '../../kingOfStrings/Listeners';

declare var games: any;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    showAlertMessage: boolean = true;
    autoplayTimer: any = null;
    isDemo: boolean = true;
    isIos: boolean = false;
    isTutorial: boolean = false;
    finalStageNumber: number = 7;
    tutorialStage: number = 1;
    visibleState: string = 'visible';
    randomLeft: number = 0;
    opponentPoints: number = 0;
    opponentPointsLoading: boolean = true;
    bottomPosition: string = '-100px';
    staticTimerFn: any = null;
    internetAvailable: boolean = true;
    isBumper: boolean = false;
    bumperType: any = 1;
    problemText: string = null;
    loadingController: any = null;
    singleNav: any = null;
    resultsArray: any = {
        rightbubbles: 0,
        wrongbubbles: 0,
        rightTreats: 0,
        wrongTreats: 0,
        totalbubbles: 0
    };
    livesRemaining: boolean = false;
    resultDeclared: boolean = false;
    persistantLives: number = 10;
    perishableLives: number = 5;

    //Change bubble bottom position
    changeBottomIndex: number = 0;

    bubbles: any = [];
    bubblesSorted: any = [];
    declaredResultData: any = {};
    popIndex: number = 0;

    gamingIndex: number = 0;

    //Timer seconds
    timerNum: number = 60;
    points: any = 0;
    bumperPoints: number = 0;
    onEnded: boolean = false;
    onLeft: boolean = false;
    problemsOver: boolean = false;
    chosenColor: string = 'red';
    bubblePopRightIndex = 0;
    bubblePopWrongIndex = 0;
    level = 0;
    boxWidth = 0;
    boxHeight = 0;
    bubbleWidthParameter = 0;
    tableMoney = 5;
    levelIncreasing: boolean = true;
    freshlyLoaded: boolean = true;
    showLoading: boolean = false;

    staticTimer: number = 2;
    staticTimerBubbleFn: any;

    //For demo
    config: any = { };

    static readonly SHOW_TUTORIAL = true;

    /*
     Config for king of strings game
     You can pass in an object to this `kosCreateConfigData` function.
     ********************************************************************
     See `IKosGameConfig.ts` for details on all valid properties.
     Any value properties will be supplied with default valies.
     See `KosGameDefaults` for all the current default values.
     ********************************************************************
    */
    kosConfig = kosCreateConfigData({
      showTutorial: HomePage.SHOW_TUTORIAL,
    });

    gameInstance: any;
    gameStyle: any = {
        width: '100%',
        height: '100%'
    };
    paramsGameId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams,
      public platform: Platform, public menu: MenuController) {
        this.paramsGameId = navParams.get('paramsGameId');
    }

    ionViewDidLoad() {
        this.calculateBoxWidth();
        this.startNow(60 * 60);
    }

    onLivesLost(gameInstance: Game) {
        console.log('Lives lost callback listened in home.ts!');
        
        console.log(`state 1, perishable lives: ${this.perishableLives}`)
        if (this.perishableLives > 0) {
            gameInstance.showLivesLost(-1);
            this.perishableLives = this.perishableLives - 1;
            console.log(`state 2, perishable lives: ${this.perishableLives}`)
          } else {
            if (this.tableMoney != 0) {
                gameInstance.showLivesLost(-1);
                this.persistantLives = this.persistantLives - 1;
            }
        }

        this.checkLivesAvailable();

        if (this.perishableLives > 0 || this.persistantLives > 0) {
          gameInstance.resurrect();
        }
    }

    /**
     * Offsets the points by the specified value. Points cannot go below zero.
     * @param points - The value by which to add/subtract from the points.
     * @param ratio - Value from 0.0 to 1.0, indicate when the note was caught.
     * If a note was missed (`points` will be negative), then `ratio` will be
     * `undefined`.
     */
    onAddPoint(points: number, ratio: number) {
      this.points = Math.max(0, this.points + points);
      console.log(`points: ${points}; note caught at: ${ratio}`);
    }

    /**
     * Disatched _just before_ the `add-points` callback. Dispatched only
     * when a note was caught.
     * @param ratio Value from 0.0 to 1.0, indicate when the note was caught.
     */
    onCaughtNote(ratio: number) {
    }

    onReady() {
        console.log('the game has finished loading and is ready to play');
    }

    onSetSeconds(seconds: number, shouldOffset = false) {
      if (shouldOffset) {
        this.timerNum += seconds;
      } else {
        this.timerNum = seconds;
      }
      console.log(`set the time (in seconds) to ${this.timerNum}`);
    }

    onTutorialClose(): void {
      console.log('tutorial-close');
    }

    onTutorialOpen(): void {
      console.log('tutorial-open');
    }

    initGameInstance() {
        // this.gameInstance = new Game(this.boxWidth, this.boxHeight);
        this.gameInstance = new Game(540, 960);
        this.gameInstance.startGame(this.kosConfig);

        this.gameInstance.listen(Listeners.ADD_POINT, this.onAddPoint.bind(this));
        this.gameInstance.listen(Listeners.CAUGHT_NOTE, this.onCaughtNote.bind(this));
        this.gameInstance.listen(Listeners.LIVES_LOST, this.onLivesLost.bind(this));
        this.gameInstance.listen(Listeners.READY, this.onReady.bind(this));
        this.gameInstance.listen(Listeners.SET_SECONDS, this.onSetSeconds.bind(this));
        this.gameInstance.listen(Listeners.TUTORIAL_OPEN, this.onTutorialOpen.bind(this));
        this.gameInstance.listen(Listeners.TUTORIAL_CLOSE, this.onTutorialClose.bind(this));
    }

    startNow(num) {
        this.gameStyle = {
            width: this.boxWidth + 'px',
            height: this.boxHeight + 'px'
        };

        switch (this.paramsGameId) {
            case 19:
                this.initGameInstance();
                break;
        }

        //Information to be used!
        this.showAlertMessage = false;
        this.onEnded = false;
        this.onLeft = false;

        this.timerNum = num; //Value of the timer

        this.perishableLives = 3; //Initial per contest lives
        this.gameInstance.startLives = this.perishableLives;

        this.persistantLives = 0; //Final bought lives

        this.points = 0; //Points scored
        
        this.checkLivesAvailable(); //This function checks if lives are available or not! It ends your contest if your lives are over.
    }

    checkLivesAvailable() {
        console.log(`checkLivesAvailable, perishable lives: ${this.perishableLives}, ` +
          `persistant lives: ${this.persistantLives}`);
        if (this.perishableLives <= 0 && this.persistantLives <= 0) {
            this.livesRemaining = false;
            this.onEnd();
        } else {
            this.livesRemaining = true;
        }
    }

    calculateBoxWidth() {
        this.boxWidth = this.platform.width();
        this.boxHeight = this.platform.height();

        // if (this.isIos) {
        //     this.boxHeight = this.boxHeight - 50;
        // }
    }

    problemsOverFn(problemText?) {
        this.problemText = 'Some problem occurred, Please try again!';
        if (problemText) {
            this.problemText = problemText;
        }
        this.timerNum = 0;
        this.problemsOver = true;
        this.clearTimer();
    }

    generateRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dismiss() {
        this.navCtrl.pop();
    }

    confirmDismissal() {
        this.showAlertMessage = false;
        this.onEnded = true;
        this.onLeft = true;
        this.clearTimer();

        setTimeout(() => {
            this.clearTimer();
        }, 1000);

        this.menu.enable(true);
    }

    clearTimer() {
        if (this.staticTimerFn) {
            clearTimeout(this.staticTimerFn);
            this.staticTimerFn = null;
        }
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    ionViewCanLeave() {
        if (!this.onEnded && !this.problemsOver && !this.isTutorial) {
        } else {
            this.menu.enable(true);
        }
    }

    _arrayRandom(len, min, max, unique) {
        var len = len ? len : 10,
            min = min !== undefined ? min : 1,
            max = max !== undefined ? max : 100,
            unique = unique ? unique : false,
            toReturn = [],
            tempObj = {},
            i = 0;

        if (unique === true) {
            for (; i < len; i++) {
                var randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
                if (tempObj['key_' + randomInt] === undefined) {
                    tempObj['key_' + randomInt] = randomInt;
                    toReturn.push(randomInt);
                } else {
                    i--;
                }
            }
        } else {
            for (; i < len; i++) {
                toReturn.push(Math.floor(Math.random() * (max - min + min)));
            }
        }

        return toReturn;
    }

    removeLoading() {
        if (this.showLoading) {
            this.showLoading = false;
            this.loadingController.dismiss();
        }
    }

    onTimerFinished() {
        this.onEnd();
    }

    onEnd() {
        if (!this.onEnded) {
            this.gameInstance.endGame();
        }
        this.clearTimer();
        this.onEnded = true;
        this.onLeft = true;

        if (this.isBumper || this.isDemo) {
            if (this.livesRemaining) {
                this.dismiss();
            }
        } else {
            if (this.livesRemaining) {
            }
        }
    }
}
