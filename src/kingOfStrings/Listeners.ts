enum Listeners {
  ADD_POINT = 'add-point',
  CAUGHT_NOTE = 'caught-note',
  LIVES_LOST = 'lives-lost',
  READY = 'ready',
  SET_SECONDS = 'set-seconds',
  TUTORIAL_OPEN = 'tutorial-open',
  TUTORIAL_CLOSE = 'tutorial-close',
}

export default Listeners;
