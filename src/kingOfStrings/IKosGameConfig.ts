// ================================================================================================
export default interface IKosGameConfig {
  /**
   * Alpha of the black background of the instructions panel.
   */
  instructionsBackAlpha?: number;

  /**
   * Number of times the screen flashes when the player loses a life.
   */
  lostLifeNumFlashes?: number;

  /**
   * Number of times the screen flashes when the player loses their last life.
   */
  lostLastLifeNumFlashes?: number;

  /**
   * Specifies the minimum progress that the note must reach before it can be caught and ranked
   * a "good" catch.
   * A note's progress is from 0 (when it starts) to 1.0 (when it reaches the bottom).
   * Note that there are three rankings: good, great, and perfect. Their values must be in ascending
   * order, with good being the lowest, great is the middle, and perfect is the highest:
   * good -> great -> perfect
   */
  noteCatchRankGood?: number;

  /**
   * Specifies the minimum progress that the note must reach before it can be caught and ranked
   * a "great" catch.
   * A note's progress is from 0 (when it starts) to 1.0 (when it reaches the bottom).
   * Note that there are three rankings: good, great, and perfect. Their values must be in ascending
   * order, with good being the lowest, great is the middle, and perfect is the highest:
   * good -> great -> perfect
   */
  noteCatchRankGreat?: number;

  /**
   * Specifies the minimum progress that the note must reach before it can be caught and ranked
   * a "perfect" catch.
   * A note's progress is from 0 (when it starts) to 1.0 (when it reaches the bottom).
   * Note that there are three rankings: good, great, and perfect. Their values must be in ascending
   * order, with good being the lowest, great is the middle, and perfect is the highest:
   * good -> great -> perfect
   */
  noteCatchRankPerfect?: number;

  /**
   * Number of points awarded for catching a note with a good ranking.
   */
  pointsGood?: number;

  /**
   * Number of points awarded for catching a note with a great ranking.
   */
  pointsGreat?: number;

  /**
   * Number of points awarded for catching a note with a perfect ranking.
   */
  pointsPerfect?: number;

   /**
   * Array of songs to play. One will be selected at random and loaded when the game starts.
   */
  songs?: string[];

  /**
   * If true, the phaser camera will shake when the last life is lost.
   */
  shouldScreenShake?: boolean;

  /**
   * If true, the game will show instructions
   */
  showTutorial?: boolean;
}
