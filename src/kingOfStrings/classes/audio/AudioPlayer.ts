export default class AudioPlayer {
  //=============================================================================================
  // properties
  //=============================================================================================
  private _game: Phaser.Game;
  
  //=============================================================================================
  // public
  //=============================================================================================
  
  //---------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this._game = game;
  }

  //---------------------------------------------------------------------------------------------
  dispose() {
    if (this._game) {
      this._game.sound.removeAll();
      this._game = null;
    }
  }
  
  //---------------------------------------------------------------------------------------------
  play(soundKey: string | string[], position?: number, stopCallback?: Function,
  stopCallbackContext?: any): Phaser.Sound {
    let key: string;

    if (Array.isArray(soundKey)) {
      key = Phaser.ArrayUtils.getRandomItem(soundKey);
    } else {
      key = soundKey;
    }

    if (!key) {
      return (null);
    }

    const sound = this._game.add.sound(key);
    if (!sound) {
      console.warn(`AudioPlayer.play. Could not play sound with key ${key}`);
      return (null);
    }

    if (stopCallback) {
      sound.onStop.addOnce(stopCallback, stopCallbackContext);
    }

    sound.play('', position);

    return (sound);
  }
}
