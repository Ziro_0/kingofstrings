import AudioPlayer from '../audio/AudioPlayer';
import NoteData from '../data/NoteData';
import Song from '../data/Song';

export default class SongPlayer {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  /**
   * Dispatched when notes are ready to be processed.
   * 
   * Event params:
   * 
   * notes - `NoteLane[]` - Specifies the lanes in which notes are being requested. Values
   *   should correspond to one of the `NoteLane` values.
   * songPlayer - `SongPlayer` - The song player that dispatched the signal.
   */
  onNotesReady: Phaser.Signal;

  private _song: Song;

  private _audio: AudioPlayer;

  private _songSound: Phaser.Sound;

  private _nextNoteIndex: number;

  private _pausedPosition: number;

  private _paused: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(audio: AudioPlayer) {
    this.onNotesReady = new Phaser.Signal();
    this._audio = audio;
    this._paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.stop();
    this.onNotesReady.dispose();
    this._audio = null;
  }

  // ----------------------------------------------------------------------------------------------
  get paused(): boolean {
    return (this._paused);
  }

  // ----------------------------------------------------------------------------------------------
  set paused(value: boolean) {
    if (!this._songSound) {
      return;
    }

    this._paused = value;
    if (this._paused) {
      this._pausedPosition = this._songSound.currentTime;
      this._songSound.pause();
    } else {
      this._songSound.resume();
    }
  }

  // ----------------------------------------------------------------------------------------------
  play(song: Song): boolean {
    if (!this._audio) {
      return (false);
    }

    this.stop();

    if (!song) {
      return (false);
    }

    this._songSound = this._audio.play(song.id);
    if (!this._songSound) {
      return (false);
    }

    this._song = song;
    this._nextNoteIndex = 0;

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  get songPosition(): number {
    return (this._songSound ? this._songSound.currentTime * 0.001 : 0);
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    if (this._songSound) {
      this._songSound.destroy();
      this._songSound = null;
    }

    this._song = null;
    this.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._songSound || this.paused) {
      return;
    }

    const noteData = this.getCurrentNoteData();
    if (!noteData) {
      return;
    }

    const requestTime = this.getNoteRequestTime(noteData);
    const soundPosition = this.songPosition;
    if (soundPosition === -1 || requestTime === -1) {
      return;
    }

    if (soundPosition >= requestTime) {
      this.onNotesReady.dispatch(noteData.hits, this);
      this.advanceNoteIndex();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private advanceNoteIndex(): void {
    const soundPosition = this.songPosition;
    if (soundPosition === -1) {
      return;
    }

    let noteRequestTime = this.getNoteRequestTime();
    while (noteRequestTime >= 0 && soundPosition >= noteRequestTime) {
      this._nextNoteIndex += 1;
      noteRequestTime = this.getNoteRequestTime();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private getCurrentNoteData(): NoteData {
    return (this._song.notes[this._nextNoteIndex]);
  }

  // ----------------------------------------------------------------------------------------------
  private getNoteRequestTime(noteData?: NoteData): number {
    if (!noteData) {
      noteData = this.getCurrentNoteData();
    }
    return (noteData ? noteData.time - this._song.noteDuration : -1);
  }
}
