class DesignData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly APP_WIDTH = 540
  
  static readonly APP_HEIGHT = 960

  /**
   * Number of consecutive note catches required to reduce the miss penalty by 1.
   * This repeats, so if you've missed a lot of notes (which will _really_ hurt your score!),
   * you can gradually reduce that miss penalty by playing well.
   */
  static readonly PENALTY_IMPUNE = 5;
};

export default DesignData;
