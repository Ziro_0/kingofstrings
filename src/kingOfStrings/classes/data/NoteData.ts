import { getNumberProperty, getProperty } from '../../util/util'
import LaneId from './LaneId';

export default class NoteData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  hits: number[];

  time: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jData?: any) {
    this.time = getNumberProperty(jData, 'time', 0);
    this.hits = NoteData.ParseHits(getProperty(jData, 'hits', 1));
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private static ParseHits(hits: number | number[]): number[] {
    if (Array.isArray(hits)) {
      return (hits);
    }

    const availableLaneIds = [
      LaneId.LEFT,
      LaneId.CENTER,
      LaneId.RIGHT,
    ];

    const out: number[] = [];
    const len = Math.min(hits, availableLaneIds.length);
    for (let index = 0; index < len; index += 1) {
      const laneId = Phaser.ArrayUtils.removeRandomItem(availableLaneIds);
      out.push(laneId);
    }

    return (out);
  }
}
