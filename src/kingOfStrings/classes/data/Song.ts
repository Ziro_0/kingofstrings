import { getNumberProperty, getProperty, getStringProperty } from '../../util/util';
import NoteData from './NoteData';

export default class Song {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  notes: NoteData[];

  private _id: string;

  private _noteDuration: number;

  private _jData: any;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jData?: any) {
    this._jData = jData;
    this._id = getStringProperty(this._jData, 'id', '');
    this._noteDuration = getNumberProperty(this._jData, 'noteDuration', 1.0);
    this.notes = Song.ParseNotes(getProperty(this._jData, 'notes', []));
  }

  // ----------------------------------------------------------------------------------------------
  get id(): string {
    return (this._id);
  }

  // ----------------------------------------------------------------------------------------------
  get noteDuration(): number {
    return (this._noteDuration);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static ParseNotes(jData: any[]): NoteData[] {
    if (!jData) {
      return ([]);
    }

    const out: NoteData[] = [];

    jData.forEach((unit) => {
      const note = new NoteData(unit);
      out.push(note);
    });

    return (out);
  }
}
