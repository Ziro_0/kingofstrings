enum LaneId {
  LEFT = 0,
  CENTER = 1,
  RIGHT = 2
};

export default LaneId;
