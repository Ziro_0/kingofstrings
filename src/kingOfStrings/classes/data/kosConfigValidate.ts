import IKosGameConfig from '../../IKosGameConfig';
import { getBooleanProperty, getNumberProperty, getProperty, getStringProperty } from '../../util/util';
import KosGameDefaults from './KosGameDefaults';

// ------------------------------------------------------------------------------------------------
export function kosConfigValidate(config: IKosGameConfig): IKosGameConfig {
  if (!config) {
    config = {};
  }

  config.instructionsBackAlpha = getNumberProperty(config, 'instructionsBackAlpha',
    KosGameDefaults.INSTRUCTIONS_BACK_ALPHA);

  config.lostLifeNumFlashes = getNumberProperty(config, 'lostLifeNumFlashes',
    KosGameDefaults.LOST_LIFE_NUM_FLASHES);

  config.lostLastLifeNumFlashes = getNumberProperty(config, 'lostLastLifeNumFlashes',
    KosGameDefaults.LOST_LAST_LIFE_NUM_FLASHES);

  config.noteCatchRankGood = getNumberProperty(config, 'noteCatchRankGood',
    KosGameDefaults.NOTE_CATCH_RANK_GOOD);

  config.noteCatchRankGreat = getNumberProperty(config, 'noteCatchRankGreat',
    KosGameDefaults.NOTE_CATCH_RANK_GREAT);

  config.noteCatchRankPerfect = getNumberProperty(config, 'noteCatchRankPerfect',
    KosGameDefaults.NOTE_CATCH_RANK_PERFECT);

  config.pointsGood = getNumberProperty(config, 'pointsGood', KosGameDefaults.POINTS_GOOD);

  config.pointsGreat = getNumberProperty(config, 'pointsGreat', KosGameDefaults.POINTS_GREAT);

  config.pointsPerfect = getNumberProperty(config, 'pointsPerfect', KosGameDefaults.POINTS_PERFECT);

  config.shouldScreenShake = getBooleanProperty(config, 'shouldScreenShake',
    KosGameDefaults.SHOULD_SCREEN_SHAKE);

  config.showTutorial = getBooleanProperty(config, 'showTutorial',
    KosGameDefaults.SHOW_TUTORIAL);

  config.songs = getProperty(config, 'songs', KosGameDefaults.SONGS);

  return (config);
}

// ------------------------------------------------------------------------------------------------
export function kosCreateConfigData(data?: IKosGameConfig): IKosGameConfig {
  if (!data) {
    data = {
      // place any custom config properties here.
      // see IKosGameConfig.ts for details on all valid properties.
    };
  }

  return (data);
}
