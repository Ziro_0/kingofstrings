export default class KosGameDefaults {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly INSTRUCTIONS_BACK_ALPHA = 0.7;

  static readonly LOST_LIFE_NUM_FLASHES = 1;

  static readonly LOST_LAST_LIFE_NUM_FLASHES = 1;

  static readonly NOTE_CATCH_RANK_GOOD = 0.85;

  static readonly NOTE_CATCH_RANK_GREAT = 0.91;

  static readonly NOTE_CATCH_RANK_PERFECT = 0.97;

  static readonly POINTS_GOOD = 1;

  static readonly POINTS_GREAT = 2;

  static readonly POINTS_PERFECT = 3;

  static readonly SHOULD_SCREEN_SHAKE = false;

  static readonly SHOW_TUTORIAL = false;

  static readonly SONGS = [
    'xtakerux-fragile-heart'
  ];
}
