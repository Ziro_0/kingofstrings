import JsonKey from '../data/JsonKey';

export default class LostLivesHeart extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly FADE_OUT_DURATION = 500;
  private static readonly FILL_COLOR = '#e75a70';
  private static readonly FONT_SIZE = 60;
  private static readonly TEXT_START_Y_OFFSET = -60;
  private static readonly TWEEN_FLOAT_OFFSET = '-150';
  private static readonly TWEEN_SCALE_DURATION = 250;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, lives: number) {
    super(game, x, y, JsonKey.IMAGES, JsonKey.LIVES_LOST_HEART);

    this.anchor.setTo(0.5);
    this.scale.setTo(0.5);

    const text = this.game.add.text(0, LostLivesHeart.TEXT_START_Y_OFFSET, lives.toString());
    text.anchor.setTo(0.5);
    text.fontSize = LostLivesHeart.FONT_SIZE;
    text.fill = LostLivesHeart.FILL_COLOR;
    this.addChild(text);

    this.game.add
      .tween(this.scale)
      .to({ x: 1, y: 1 }, LostLivesHeart.TWEEN_SCALE_DURATION, Phaser.Easing.Bounce.In, true)
      .onComplete.add(() => {
        this.game.add
          .tween(this)
          .to({ y: LostLivesHeart.TWEEN_FLOAT_OFFSET, alpha: 0 }, LostLivesHeart.FADE_OUT_DURATION,
            Phaser.Easing.Linear.None, true)
          .onComplete.add(() => {
            this.destroy();
          });
      });
  }
}
