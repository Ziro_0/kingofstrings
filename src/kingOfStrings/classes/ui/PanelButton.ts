import JsonKey from '../data/JsonKey';
import LaneId from '../data/LaneId';

// ================================================================================================
interface IButtonFrames {
  id: LaneId;
  frameOff: string;
  frameOn: string;
  shineFrame: string;
}

// ================================================================================================
export default class PanelButton extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BUTTON_FRAMES: IButtonFrames[] = [
    {
      id: LaneId.LEFT,
      frameOff: JsonKey.PANEL_BUTTON_RED_OFF,
      frameOn: JsonKey.PANEL_BUTTON_RED_ON,
      shineFrame: JsonKey.SHINE_RED,
    },

    {
      id: LaneId.CENTER,
      frameOff: JsonKey.PANEL_BUTTON_BLUE_OFF,
      frameOn: JsonKey.PANEL_BUTTON_BLUE_ON,
      shineFrame: JsonKey.SHINE_BLUE,
    },

    {
      id: LaneId.RIGHT,
      frameOff: JsonKey.PANEL_BUTTON_YELLOW_OFF,
      frameOn: JsonKey.PANEL_BUTTON_YELLOW_ON,
      shineFrame: JsonKey.SHINE_YELLOW,
    },
  ];

  private static readonly Y_POSITION = 856;

  private _frameData: IButtonFrames;

  private _laneId: LaneId;

  private _shineImage: Phaser.Image;

  private _button: Phaser.Button;

  private _timer: Phaser.Timer;

  private _timerEvent: Phaser.TimerEvent;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, laneId: LaneId, parent: PIXI.DisplayObjectContainer) {
    super(game, parent);

    this.x = x;
    this.y = PanelButton.Y_POSITION;

    this._laneId = laneId;

    this._frameData = PanelButton.BUTTON_FRAMES.find((x) => x.id === this.laneId);

    this._timer = this.game.time.create(false);
    this._timer.start();

    this.initButton();
  }

  // ----------------------------------------------------------------------------------------------
  get laneId(): LaneId {
    return (this._laneId);
  }

  // ----------------------------------------------------------------------------------------------
  lightUp(): void {
    const frame = this._frameData;
    this._button.setFrames(frame.frameOn, frame.frameOn, frame.frameOn, frame.frameOn);

    if (this._timerEvent) {
      this._timer.remove(this._timerEvent);
    }

    const DELAY = 250;
    this._timerEvent = this._timer.add(DELAY, () => {
      this._button.setFrames(frame.frameOff, frame.frameOff, frame.frameOn, frame.frameOff);
      this._timerEvent = null;
    });
  }

  // ----------------------------------------------------------------------------------------------
  shine(): void {
    if (!this._shineImage) {
      const frame = this._frameData;
      this._shineImage = this.game.add.image(0, 0, JsonKey.IMAGES, frame.shineFrame, this);
      
      const AX = 0.5;
      const AY = 0.742
      this._shineImage.anchor.set(AX, AY);
    }

    this._shineImage.alpha = 1.0;
    
    const DURATION = 300;
    this.game.tweens.create(this._shineImage).to(
      {
        alpha: 0,
      },
      DURATION,
      Phaser.Easing.Linear.None,
      true);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initButton(): void {
    const frame = this._frameData;
    this._button = this.game.add.button(0, 0, JsonKey.IMAGES, null, this,
      frame.frameOff, frame.frameOff, frame.frameOn, frame.frameOff, this);
      this._button.anchor.set(0.5);
  }
}
