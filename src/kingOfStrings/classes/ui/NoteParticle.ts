import JsonKey from '../data/JsonKey';
import Note from './Note';

// ================================================================================================
export default class NoteParticle extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _ticks: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, note: Note) {
    super(game, note.x, note.y, JsonKey.IMAGES, note.frameName);
    this.scale.set(note.scale.x, note.scale.y);
    this.anchor.set(note.anchor.x, note.anchor.y);
    this._ticks = 25;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    super.update();

    this._ticks -= 1;
    if (this._ticks <= 0) {
      this.destroy();
    } else {
      this.visible = !this.visible;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
}
