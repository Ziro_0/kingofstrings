import JsonKey from '../data/JsonKey';

export default class ModalDialog extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  onComplete: Phaser.Signal;

  shouldCloseOnComplete: boolean;

  private _messages: string[];

  private _text: Phaser.BitmapText;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, parent: PIXI.DisplayObjectContainer, dimBackAlpha: number) {
    super(game, parent);

    this.onComplete = new Phaser.Signal();

    this.initInput();

    this.createDimBack(dimBackAlpha);
    this.createText();

    this.shouldCloseOnComplete = true;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onComplete.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  present(message: string | string[], fontSize?: number) {
    if (Array.isArray(message)) {
      this._messages = message.concat();
    } else {
      this._messages = [ message ];
    }

    if (fontSize) {
      this._text.fontSize = fontSize;
    }

    if (!this.presentNextMessage()) {
      this.complete();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private complete(): void {
    if (this.shouldCloseOnComplete) {
      this.visible = false;
    }

    this.onComplete.dispatch(this);
  }

  // ----------------------------------------------------------------------------------------------
  private createDimBack(dimBackAlpha: number): void {
    const graphics = this.game.add.graphics(0, 0, this);
    graphics.beginFill(0x000000, dimBackAlpha);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();
  }

  // ----------------------------------------------------------------------------------------------
  private createText(): void {
    this._text = this.game.add.bitmapText(
      this.game.width / 2,
      this.game.height / 2,
      JsonKey.DIALOG_FONT);

    this._text.align = 'center';

    const MARGIN = 16;
    this._text.maxWidth = this.game.width - MARGIN * 2;

    this._text.anchor.set(0.5, 0.5);
    this.add(this._text);
  }

  // ----------------------------------------------------------------------------------------------
  private initInput(): void {
    this.inputEnableChildren = true;

    this.onChildInputUp.add(() => {
      if (!this.presentNextMessage()) {
        this.complete();
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private presentNextMessage(): boolean {
    const message = this._messages.shift();
    if (!message) {
      return (false);
    }

    this.visible = true;
    this._text.text = message;
    return (true);
  }
}
