import DesignData from '../data/DesignData';
import JsonKey from '../data/JsonKey';

// ================================================================================================
export default class BannerMessage extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, frame: string) {
    super(game, 0, 0, JsonKey.IMAGES, frame);
    this.anchor.set(0.5);
    this.setupTweens();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private setupTweens(): void {
    const PRESENT_DURATION = 125;
    const UNPRESENT_DURATION = 250;
    const UNPRESENT_DELAY = 250;
    const UNPRESENT_FLOAT_OFS = '-50';
    
    const tween1 = this.game.add.tween(this);
    tween1.from(
      {
        alpha: 0,
      },
      PRESENT_DURATION,
      Phaser.Easing.Linear.None,
      true);

    tween1.onComplete.addOnce(() => {
      const tween2 = this.game.add.tween(this);
      tween2.to(
        {
          alpha: 0,
          y: UNPRESENT_FLOAT_OFS,
        },
        UNPRESENT_DURATION,
        Phaser.Easing.Linear.None,
        true,
        UNPRESENT_DELAY,
      );
      
      tween2.onComplete.addOnce(() => {
        this.destroy();
      }, this);
    });
  }
}