import LaneId from '../data/LaneId';

// ================================================================================================
export default class KeysHandler {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  game: Phaser.Game;

  onKeyDown: Phaser.Signal;

  private _leftKey: Phaser.Key;

  private _centerKey: Phaser.Key;

  private _rightKey: Phaser.Key;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;

    this.onKeyDown = new Phaser.Signal();

    this.initKeys();
  }

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onKeyDown.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (this._leftKey.justDown) {
      this.onKeyDown.dispatch(LaneId.LEFT, this);
    }

    if (this._centerKey.justDown) {
      this.onKeyDown.dispatch(LaneId.CENTER, this);
    }

    if (this._rightKey.justDown) {
      this.onKeyDown.dispatch(LaneId.RIGHT, this);
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private initKey(keyCode: number): Phaser.Key {
    const key = this.game.input.keyboard.addKey(keyCode);
    return (key);
  }

  // ----------------------------------------------------------------------------------------------
  private initKeys(): void {
    this._leftKey = this.initKey(Phaser.KeyCode.ONE);
    this._centerKey = this.initKey(Phaser.KeyCode.TWO);
    this._rightKey = this.initKey(Phaser.KeyCode.THREE);
  }
}
