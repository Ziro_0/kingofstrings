import LaneId from '../data/LaneId';

// ================================================================================================
interface IBoardLane {
  id: LaneId;
  noteXStart: number;
  noteXEnd: number;
}

export default IBoardLane;
