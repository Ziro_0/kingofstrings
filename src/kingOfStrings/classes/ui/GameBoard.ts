import IKosGameConfig from '../../IKosGameConfig';
import JsonKey from '../data/JsonKey';
import LaneId from '../data/LaneId';
import IBoardLane from './IBoardLane';
import Note from './Note';
import PanelButton from './PanelButton';

// ================================================================================================
type buttonCallback = () => void;

// ================================================================================================
export default class GameBoard extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly LANES: IBoardLane[] = [
    {
      id: LaneId.LEFT,
      noteXStart: 246,
      noteXEnd: 87,
    },

    {
      id: LaneId.CENTER,
      noteXStart: 270,
      noteXEnd: 270,
    },

    {
      id: LaneId.RIGHT,
      noteXStart: 292,
      noteXEnd: 453,
    },
  ];

  /**
   * Dispatched if the player catches a note.
   * 
   * Event params:
   * 
   * note: `Note`. The note that was caught.
   * 
   * board: `GameBoard`: The game board that dispatched the signal.
   */
   onNoteCaught: Phaser.Signal;

  /**
   * Dispatched if the player missed a note, either because it reached the bottom uncaught,
   * of the player pressed a button and missed.
   * 
   * Event params:
   * 
   * note: `Note`. The note that reached the bottom. If this is `null`, that means the player
   * missed.
   * 
   * board: `GameBoard`: The game board that dispatched the signal.
   */
  onNoteMissed: Phaser.Signal;

  onNoteRecorded: Phaser.Signal;

  noteDuration: number;

  isRecordMode: boolean;

  private _buttons: Map<LaneId, PanelButton>;

  private _kosConfig: IKosGameConfig

  private _notesGroup: Phaser.Group;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, kosConfig: IKosGameConfig, parent: PIXI.DisplayObjectContainer) {
    super(game, parent);

    this.onNoteCaught = new Phaser.Signal();
    this.onNoteMissed = new Phaser.Signal();
    this.onNoteRecorded = new Phaser.Signal();

    this.noteDuration = 0;

    this._notesGroup = this.game.add.group(this);

    this._kosConfig = kosConfig;

    this.initButtonsPanel();
    this.initPanelButtons()
  }

  // ----------------------------------------------------------------------------------------------
  addNotes(noteLanes: LaneId[], startSongPosition: number): void {
    if (!noteLanes) {
      return;
    }

    noteLanes.forEach((noteLane) => {
      const lane = GameBoard.LANES[noteLane];
      if (lane) {
        this.addNote(lane, startSongPosition);
      }
    })
  }

  // ----------------------------------------------------------------------------------------------
  checkLane(laneId: LaneId): void {
    const note = this.getCaughtNote(laneId);
    if (note) {
      this.caughtNote(note);
    } else {
      this.onNoteMissed.dispatch(null, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onNoteCaught.dispose();
    this.onNoteMissed.dispose();
    this.onNoteRecorded.dispose();
  }

  // ----------------------------------------------------------------------------------------------
  lightUpButton(laneId: LaneId): void {
    const button = this._buttons.get(laneId);
    if (button) {
      button.lightUp();
    }
  }

  // ----------------------------------------------------------------------------------------------
  shineButton(laneId: LaneId): void {
    const button = this._buttons.get(laneId);
    if (button) {
      button.shine();
    }
  }

  // ----------------------------------------------------------------------------------------------
  step(songPosition: number): void {
    this._notesGroup.forEach((note: Note) => {
      note.step(songPosition);
    });
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this.inputEnableChildren = false;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addNote(lane: IBoardLane, startSongPosition: number): void {
    const note = new Note(this.game, lane, this.noteDuration, startSongPosition);
    note.onComplete.addOnce(this.onNoteCompleted, this);
    this._notesGroup.add(note);
  }

  // ----------------------------------------------------------------------------------------------
  private caughtNote(note: Note) {
    this.onNoteCaught.dispatch(note, this);
    note.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private getCaughtNote(laneId: LaneId): Note {
    for (let index = 0; index < this._notesGroup.length; index += 1) {
      const note = <Note> this._notesGroup.getAt(index);

      if (!(note instanceof Note)) {
        // sanity check
        continue;
      }

      if (note.lane.id !== laneId) {
        // note must be in the specified lane
        continue;
      }

      return (note);
    }

    return (null);
  }

  // ----------------------------------------------------------------------------------------------
  private initPanelButton(x: number, laneId: LaneId, callback: buttonCallback): PanelButton {
    const button = new PanelButton(this.game, x, laneId, this);
    button.onChildInputDown.add(() => {
      callback.call(this);
    });
    
    return (button);
  }

  // ----------------------------------------------------------------------------------------------
  private initPanelButtons(): void {
    this._buttons = new Map();

    const LEFT_POSITION = 100;
    this._buttons.set(LaneId.LEFT,
      this.initPanelButton(LEFT_POSITION, LaneId.LEFT, this.onLeftButtonPressed));
    
    const CENTER_POSITION = 269;
    this._buttons.set(LaneId.CENTER,
      this.initPanelButton(CENTER_POSITION, LaneId.CENTER,  this.onCenterButtonPressed));

    const RIGHT_POSITION = 438;
    this._buttons.set(LaneId.RIGHT,
      this.initPanelButton(RIGHT_POSITION, LaneId.RIGHT, this.onRightButtonPressed));
  }

  // ----------------------------------------------------------------------------------------------
  private initButtonsPanel(): void {
    const X_PANEL = 0;
    const Y_PANEL = 857;
    this.game.add.image(X_PANEL, Y_PANEL, JsonKey.IMAGES, JsonKey.BUTTONS_PANEL, this);
  }

  // ----------------------------------------------------------------------------------------------
  private onCenterButtonPressed(): void {
    if (this.isRecordMode) {
      this.onNoteRecorded.dispatch(LaneId.CENTER, this);
    } else {
      this.checkLane(LaneId.CENTER);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onLeftButtonPressed(): void {
    if (this.isRecordMode) {
      this.onNoteRecorded.dispatch(LaneId.LEFT, this);
    } else {
      this.checkLane(LaneId.LEFT);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onRightButtonPressed(): void {
    if (this.isRecordMode) {
      this.onNoteRecorded.dispatch(LaneId.RIGHT, this);
    } else {
      this.checkLane(LaneId.RIGHT);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onNoteCompleted(note: Note): void {
    this.onNoteMissed.dispatch(note, this);
  }
}