import JsonKey from '../data/JsonKey';
import LaneId from '../data/LaneId';
import IBoardLane from './IBoardLane';

// ================================================================================================
interface IImageFrame {
  laneId: LaneId;
  frame: string;
}

// ================================================================================================
export default class Note extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly IMAGE_FRAMES: IImageFrame[] = [
    {
      laneId: LaneId.LEFT,
      frame: JsonKey.NOTE_RED,
    },

    {
      laneId: LaneId.CENTER,
      frame: JsonKey.NOTE_BLUE,
    },

    {
      laneId: LaneId.RIGHT,
      frame: JsonKey.NOTE_YELLOW,
    },
  ];

  private static readonly SCALE_END = 1.0;
  
  private static readonly SCALE_START = 0.20;

  private static readonly Y_END = 932;

  private static readonly Y_START = -16;

  onComplete: Phaser.Signal;

  readonly lane: IBoardLane;

  private _totalDuration: number;

  private _startSongPosition: number;

  private _progress: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, lane: IBoardLane, duration: number,
    startSongPosition: number) {
    super(game, lane.noteXStart, Note.Y_START, JsonKey.IMAGES, Note.GetFrame(lane.id));

    this.onComplete = new Phaser.Signal();

    this.lane = lane;
    this._totalDuration = duration;
    this._startSongPosition = startSongPosition;

    this.anchor.set(0.5);
    this.scale.set(Note.SCALE_START);

    this._progress = 0;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.onComplete.dispose();
    super.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  get progress(): number {
    return (this._progress);
  }

  // ----------------------------------------------------------------------------------------------
  step(songPosition: number): void {
    this._progress = (songPosition - this._startSongPosition) / this._totalDuration;
    if (this._progress >= 1.0) {
      this.complete();
    } else {
      this.advance();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private advance(): void {
    const progress = this._progress;
    this.x = Phaser.Math.linear(this.lane.noteXStart, this.lane.noteXEnd, progress);
    this.y = Phaser.Math.linear(Note.Y_START, Note.Y_END, progress);
    this.scale.set(Phaser.Math.linear(Note.SCALE_START, Note.SCALE_END, progress));
  }

  // ----------------------------------------------------------------------------------------------
  private complete(): void {
    this.onComplete.dispatch(this);
    this.destroy();
  }

  // ----------------------------------------------------------------------------------------------
  private static GetFrame(laneId: LaneId): string {
    const data = Note.IMAGE_FRAMES.find((x) => x.laneId === laneId);
    if (data) {
      return (data.frame);
    }

    return (Phaser.ArrayUtils.getRandomItem(Note.IMAGE_FRAMES).frame);
  }
}