import DesignData from '../classes/data/DesignData';
import ISceneData from '../classes/data/ISceneData';

export default class BootState {
    game: Phaser.Game;

    constructor(game: Phaser.Game) {
      this.game = game;
    }

    init() {
      // Responsive scaling
      this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

      // Center the game
      // this.game.scale.pageAlignHorizontally = true;
      this.game.scale.pageAlignVertically = true;

      // Portrait orientation
      this.game.scale.forceOrientation(false, true);
    }

    preload() {
      this.game.load.image('preload_bar', 'assets/game-piano/images/preload_bar.png');
    }

    create() {
      const sceneData: ISceneData = {
        scale: this.calcAppScale(),
      };

      this.game.state.start('PreloadState', true, false, sceneData);
    }

    private calcAppScale(): number {
      const xs = this.game.width / DesignData.APP_WIDTH;
      const ys = this.game.height / DesignData.APP_HEIGHT;
      console.log(DesignData.APP_WIDTH, this.game.width, xs);
      console.log(DesignData.APP_HEIGHT, this.game.height, ys);
      return (Math.min(xs, ys));
    }
}
