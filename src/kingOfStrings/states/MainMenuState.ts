import ISceneData from '../classes/data/ISceneData';

export default class MainMenuState {
  game: Phaser.Game;

  sceneData: ISceneData;

  constructor(game) {
    this.game = game;
  }

  init(sceneData: ISceneData) {
    this.sceneData = sceneData;
  }

  create() {
    console.log('MainMenu state [create] started');
    this.game.state.start('GameState', true, false, this.sceneData);
  }
}
