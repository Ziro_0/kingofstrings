import AudioPlayer from '../classes/audio/AudioPlayer';
import DesignData from '../classes/data/DesignData';
import ISceneData from '../classes/data/ISceneData';
import JsonKey from '../classes/data/JsonKey';
import LaneId from '../classes/data/LaneId';
import Songs from '../classes/data/Songs';
import SongPlayer from '../classes/songPlayer/SongPlayer';
import BannerMessage from '../classes/ui/BannerMessage';
import GameBoard from '../classes/ui/GameBoard';
import KeysHandler from '../classes/ui/KeysHandler';
import ModalDialog from '../classes/ui/ModalDialog';
import Note from '../classes/ui/Note';
import NoteParticle from '../classes/ui/NoteParticle';
import UiFlash from '../classes/ui/UiFlash';
import { Game } from '../game';
import Listeners from '../Listeners';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly DESIGN_WIDTH = 540;

  static readonly DESIGN_HEIGHT = 960;

  static readonly INSTRUCTIONS_FONT_DESIGN_SIZE = 72;

  private static readonly IS_RECORD_MODE = false;

  private _sceneData: ISceneData;

  private _gameBoard: GameBoard;

  private _songs: Songs;

  private _songPlayer: SongPlayer;

  private _keysHandler: KeysHandler;
  
  private _audioPlayer: AudioPlayer;

  private _dialog: ModalDialog;

  private _banner: BannerMessage;

  private _backgroundGroup: Phaser.Group;

  private _boardGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _timer: Phaser.Timer;

  private _missPenalty: number;

  private _numConsecutiveCatches: number;

  private _isGameOver: boolean;

  private _hasGameStarted: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();
      this.initGroups();
      this.initBackground();
      this.initUi();

      this.setupGameBoard();

      this.presentUiFlash(1, 0);

      if (this.shouldShowInstructions()) {
        this.presentInstructionsDialog();
      } else {
        this.startGame();
      }

      this.listenerCallback(Listeners.READY, this.game);
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  init(sceneData: ISceneData): void {
    this._sceneData = sceneData;
    this.initAudioPlayer();
    this.createSongPlayer();
    this.createSongs();
  }

  // ----------------------------------------------------------------------------------------------
  listenerCallback(key: string, ...args): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this._keysHandler.destroy();
    this.audio.dispose();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.updateKeys();
    this.updateSong();
    this.updateGameBoard();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private createSongPlayer(): void {
    this._songPlayer = new SongPlayer(this.audio);
    this._songPlayer.onNotesReady.add(this.onSongPlayerNotesReady, this);
  }
  
  // ----------------------------------------------------------------------------------------------
  private createSongs(): void {
    const jData = this.game.cache.getJSON(JsonKey.SONGS);
    if (!jData) {
      console.warn(`Songs JSON "${JsonKey.SONGS}" not found in cache.`)
    }

    this._songs = new Songs(jData);
  }

  // ----------------------------------------------------------------------------------------------
  private handleKeyPress(laneId: LaneId): void {
    this._gameBoard.lightUpButton(laneId);
    this._gameBoard.checkLane(laneId);
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    // this.game.paused = false;
    // this._songPlayer.paused = false;
    this.presentResumeDialog();
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    this.add.image(0, 0, JsonKey.IMAGES, JsonKey.BACKGROUND, this._backgroundGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    return (this.game.add.group());
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._boardGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initKeys(): void {
    this._keysHandler = new KeysHandler(this.game);
    this._keysHandler.onKeyDown.add((laneId: LaneId) => {
      this.handleKeyPress(laneId);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initKeys();
  }

  // ----------------------------------------------------------------------------------------------
  private notifyPointsLost(): void {
    this.listenerCallback(Listeners.ADD_POINT, this._missPenalty);
  }

  // ----------------------------------------------------------------------------------------------
  private onGameBoardNoteCaught(note: Note): void {
    this.presentBannerMessage(note.progress);
    this.addPoints(note.progress);
    this.presentNoteCaughtParticle(note);
    this.updateScoringMetrics();
    this._gameBoard.shineButton(note.lane.id);
  }

  // ----------------------------------------------------------------------------------------------
  private addPoints(noteProgress: number): void {
    const kosConfig = this.gameObject.kosConfig;
    let points: number;
    if (noteProgress >= kosConfig.noteCatchRankPerfect) {
      points = kosConfig.pointsPerfect;
    } else if (noteProgress >= kosConfig.noteCatchRankGreat) {
      points = kosConfig.pointsGreat;
    } else {
      points = kosConfig.pointsGood;
    }

    this.listenerCallback(Listeners.CAUGHT_NOTE, noteProgress);
    this.listenerCallback(Listeners.ADD_POINT, points, noteProgress);
  }

  // ----------------------------------------------------------------------------------------------
  private onGameBoardNoteMissed(): void {
    this.presentUiFlash(this.gameObject.kosConfig.lostLifeNumFlashes);
    this.presentScreenTremor();
    this.notifyPointsLost();
    this.updateMissPenalty();
    this._numConsecutiveCatches = 0;
  }

  // ----------------------------------------------------------------------------------------------
  private onGameBoardNoteRecorded(laneId: LaneId): void {
    console.log(this._songPlayer.songPosition, laneId);
  }

  // ----------------------------------------------------------------------------------------------
  private onSongPlayerNotesReady(notes: LaneId[]): void {
    this._gameBoard.addNotes(notes, this._songPlayer.songPosition);
  }

  // ----------------------------------------------------------------------------------------------
  presentBannerMessage(noteProgress: number): void {
    const kosConfig = this.gameObject.kosConfig;

    if (this._banner) {
      this._banner.destroy();
    }

    let banner: BannerMessage;
    if (noteProgress >= kosConfig.noteCatchRankPerfect) {
      banner = new BannerMessage(this.game, JsonKey.BANNER_PERFECT);
    } else if (noteProgress >= kosConfig.noteCatchRankGreat) {
      banner = new BannerMessage(this.game, JsonKey.BANNER_GREAT);
    } else {
      banner = new BannerMessage(this.game, JsonKey.BANNER_GOOD);
    }

    banner.x = this.game.world.centerX;
    banner.y = this.calcViewOffset() + 180;

    this.uiGroup.add(banner);

    this._banner = banner;
  }
  
  // ----------------------------------------------------------------------------------------------
  private presentInstructionsDialog(): void {
    const message = 'Catch all the falling notes! Tap screen to start.';
    const logicalScale = this.game.width / GameState.DESIGN_WIDTH;
    console.log('font logical scale: ', logicalScale)
    const FONT_SIZE = GameState.INSTRUCTIONS_FONT_DESIGN_SIZE * logicalScale;
    this.listenerCallback(Listeners.TUTORIAL_OPEN);
    this.presentModalDialog(message,
      () => {
        this._hasGameStarted = true;
        this.listenerCallback(Listeners.TUTORIAL_CLOSE);
        this.startGame();
      },
      FONT_SIZE);
  }

  // ----------------------------------------------------------------------------------------------
  private presentModalDialog(message: string | string[], callback: Function, fontSize?: number,
    context?: any): void {
    if (!this._dialog) {
      const dimBackAlpha = this.gameObject.kosConfig.instructionsBackAlpha;
      this._dialog = new ModalDialog(this.game, this.uiGroup, dimBackAlpha);
    }

    this._dialog.onComplete.removeAll();
    this._dialog.onComplete.addOnce(callback, context || this);
    this._dialog.present(message, fontSize);
  }

  // ----------------------------------------------------------------------------------------------
  private presentNoteCaughtParticle(note: Note): void {
    if (note.progress < this.gameObject.kosConfig.noteCatchRankGood) {
      const noteParticle = new NoteParticle(this.game, note);
      this._backgroundGroup.add(noteParticle);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private presentResumeDialog(): void {
    const message = 'Tap screen to continue.';
    const logicalScale = this.game.width / GameState.DESIGN_WIDTH;
    const FONT_SIZE = GameState.INSTRUCTIONS_FONT_DESIGN_SIZE * logicalScale;
    this.presentModalDialog(message,
      () => {
        this._songPlayer.paused = false;

        if (!this._hasGameStarted) {
          this._hasGameStarted = true;
          this.listenerCallback(Listeners.TUTORIAL_CLOSE);
          this.startGame();
        }
      },
      FONT_SIZE);
  }

  // ----------------------------------------------------------------------------------------------
  private presentScreenTremor(): void {
    const config = this.gameObject.kosConfig;
    
    if (!config.shouldScreenShake) {
      return;
    }

    const INTENSITY = 0.02;
    const DURATION = config.lostLastLifeNumFlashes *
      (UiFlash.DEFAULT_FLASH_ON_DURATION + UiFlash.DEFAULT_FLASH_OFF_DURATION);
    this.camera.shake(INTENSITY, DURATION);
  }

  // ----------------------------------------------------------------------------------------------
  private presentUiFlash(numFlashes: number, alpha = 1.0): void {
    const uiFlash = new UiFlash(this.game, numFlashes);
    uiFlash.alpha = alpha;
    this.uiGroup.add(uiFlash);
  }

  // ----------------------------------------------------------------------------------------------
  private playSong(): void {
    const song = Phaser.ArrayUtils.getRandomItem(this._songs.data);
    this._gameBoard.noteDuration = song.noteDuration;
    this._songPlayer.play(song);
  }

  // ----------------------------------------------------------------------------------------------
  private setupGameBoard(): void {
    this._gameBoard = new GameBoard(this.game, this.gameObject.kosConfig, this._boardGroup);

    this._gameBoard.isRecordMode = GameState.IS_RECORD_MODE;
    if (this._gameBoard.isRecordMode) {
      this._gameBoard.onNoteRecorded.add(this.onGameBoardNoteRecorded, this);
    } else {
      this._gameBoard.onNoteCaught.add(this.onGameBoardNoteCaught, this);
      this._gameBoard.onNoteMissed.add(this.onGameBoardNoteMissed, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this._songPlayer.paused = true;
    // this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, JsonKey.IMAGES,
      JsonKey.INCORRECT_ORIENTATION_MESSAGE, this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;
    
    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private shouldShowInstructions(): boolean {
    return (this.gameObject.kosConfig.showTutorial === true);
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    const START_TIME = 60;
    this.listenerCallback(Listeners.SET_SECONDS, START_TIME);
    this._missPenalty = -1;
    this._numConsecutiveCatches = 0;
    this.playSong();
  }

  // ----------------------------------------------------------------------------------------------
  private updateGameBoard(): void {
    if (this._gameBoard) {
      this._gameBoard.step(this._songPlayer.songPosition);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateKeys(): void {
    if (this._keysHandler) {
      this._keysHandler.update();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateMissPenalty(): void {
    this._missPenalty -= 2;
  }

  // ----------------------------------------------------------------------------------------------
  private updateScoringMetrics(): void {
    this._numConsecutiveCatches += 1;

    if (this._numConsecutiveCatches >= DesignData.PENALTY_IMPUNE) {
      this._missPenalty = Math.min(-1, this._missPenalty + 1);
      this._numConsecutiveCatches = 0;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateSong(): void {
    if (!GameState.IS_RECORD_MODE) {
      this._songPlayer.update();
    }
  }
}
