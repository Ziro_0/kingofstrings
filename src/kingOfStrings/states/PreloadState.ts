import ISceneData from '../classes/data/ISceneData';
import JsonKey from '../classes/data/JsonKey';
import { Game } from '../game';

export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-king-of-strings/';

  private _game: Phaser.Game;
  
  private _preloadBar: Phaser.Image;

  private _sceneData: ISceneData;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this._game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this._game.state.start('MainMenuState', true, false, this._sceneData);
  }

  // ----------------------------------------------------------------------------------------------
  init(sceneData: ISceneData): void {
    this._sceneData = sceneData;
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this._preloadBar = this._game.add.image(
      this._game.world.centerX,
      this._game.world.centerY,
      JsonKey.PRELOAD_BAR);
    this._preloadBar.anchor.set(0.5);

    this._game.load.setPreloadSprite(this._preloadBar);

    // load assets
    this.loadAtlases();
    this.loadJsonFiles();
    this.loadSounds();
    this.loadFonts();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private getSongKeys(): string[] {
    const game = this._game as Game;
    return (game.kosConfig.songs);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlas(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}atlases/${key}`;
    this._game.load.atlas(key, `${baseUrl}.png`, `${baseUrl}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlases(): void {
    [
      JsonKey.IMAGES,
    ].forEach((key) => {
      this.loadAtlas(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadFont(key: string): void {
    const path = `${PreloadState.BASE_PATH}fonts/`;
    this._game.load.bitmapFont(key, `${path}${key}.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFontBfg(key: string): void {
    // Loads fonts created by Bitmap Font Generator. BFG appends a "_x"-like template onto the 
    // ends of its image files, which doesn't exactly match key (which doesn't use the template).
    const path = `${PreloadState.BASE_PATH}fonts/`;
    this._game.load.bitmapFont(key, `${path}${key}_0.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFonts(): void {
    [
    ].forEach((key) => {
      this.loadFont(key);
    });

    [
      JsonKey.DIALOG_FONT,
    ].forEach((key) => {
      this.loadFontBfg(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    const path = `${PreloadState.BASE_PATH}json/`;
    this._game.load.json(key, `${path}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsonFiles(): void {
    [
      JsonKey.SONGS,
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}sounds/${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl
    ];

    this._game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    const sounds = this.getSongKeys().concat([
      // place sound keys to load here
    ]);
    
    sounds.forEach((key) => {
      this.loadSound(key);
    });
  }
}